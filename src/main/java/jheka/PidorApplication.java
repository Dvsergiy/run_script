package jheka;

import java.io.InputStream;
import java.util.Properties;

import jheka.servlet.WebInitializer;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.annotations.ClassInheritanceHandler;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.springframework.web.WebApplicationInitializer;

public class PidorApplication {

	public static void main(String... args) throws Exception {
		Properties properties = new Properties();
		InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream("webapp/application.properties");
		properties.load(stream);
		stream.close();

		WebAppContext webAppContext = new WebAppContext();
		webAppContext.setResourceBase("resource");
		webAppContext.setContextPath(properties.getProperty("base.url"));
		webAppContext.setAttribute("script_path", properties.getProperty("script_pat"));
		webAppContext.setConfigurations(new Configuration[]{
				new WebXmlConfiguration(),
				new AnnotationConfiguration() {
					@Override
					public void preConfigure(WebAppContext context) {
						ClassInheritanceMap map = new ClassInheritanceMap();
						map.put(WebApplicationInitializer.class.getName(), new ConcurrentHashSet<String>() {{
							add(WebInitializer.class.getName());
						}});
						context.setAttribute(CLASS_INHERITANCE_MAP, map);
						_classInheritanceHandler = new ClassInheritanceHandler(map);
					}
				}
		});
		Server server = new Server();
		server.setHandler(webAppContext);
		server.setConnectors(new Connector[]{buildConnector(server)});
		server.start();
		server.join();
	}

	private static Connector buildConnector(Server server) {
		int port = 18080;
		ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(new HttpConfiguration()));
		http.setPort(port);
		return http;
	}
}
