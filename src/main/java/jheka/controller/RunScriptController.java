package jheka.controller;

import java.io.IOException;

import jheka.model.RunScriptRequest;
import jheka.service.RunScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class RunScriptController {
    @Autowired
    public RunScriptService runScriptService;
    /**
     * запрос идет по адресу http://твой_сайт/run_script
     * @param runScriptRequest тут xml типо будет
     * @return
     */
    @RequestMapping(path = "/run_script",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
           )
    @ResponseBody
    public FileSystemResource runScript(@RequestBody RunScriptRequest runScriptRequest)
    {
        return runScriptService.runScript(runScriptRequest.getValue());
    }

    @RequestMapping(path = "/run_script_get/{text}",
            method = RequestMethod.GET
            )
    @ResponseBody
    public HttpEntity runScript(@PathVariable("text") String text)
    {
        FileSystemResource file = runScriptService.runScript(text);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_PDF);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=" + file.getFilename().replace(" ", "_"));
        try {
            header.setContentLength(file.contentLength());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new HttpEntity<>(file, header);
    }
}
