package jheka.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RunScriptRequest {
    @JsonProperty("xml")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
