package jheka.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.UUID;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

@Service
public class RunScriptService {

    public static final String TMP = "/tmp/";

    public FileSystemResource runScript(String xml) {
        String inputXmlPath = saveInputFile(xml);
        String outputFilePath = runPythonScript(inputXmlPath);
        return new FileSystemResource(outputFilePath);
    }

    private String runPythonScript(String xmlPath) {
        StringWriter writer = new StringWriter();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptContext context = new SimpleScriptContext();

        context.setWriter(writer);
        context.setAttribute("xml", xmlPath,ScriptContext.ENGINE_SCOPE);
        ScriptEngine engine = manager.getEngineByName("python");
        try {
            engine.eval(new FileReader(System.getProperty("script_path")), context);
        } catch (ScriptException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    private String saveInputFile(String xml){
        String xmlPath = TMP + UUID.randomUUID().toString() + ".xml";
        try (PrintWriter out = new PrintWriter(xmlPath)) {
            out.println(xml);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return xmlPath;
    }
}
